fun main() {
    fun part1(input: List<String>): Int {
        val visibleTrees = mutableSetOf<Pair<Int, Int>>()
        val forest = input.map { it.toList() }

        for (y in forest.indices) {
            var previous = '0' - 1
            val row = forest[y]
            for (x in row.indices) {
                if (row[x] > previous) {
                    visibleTrees.add(x to y)
                } else {
                    continue
                }
                previous = row[x]
            }
            previous = '0' - 1
            for (x in row.lastIndex downTo 0) {
                if (row[x] > previous) {
                    visibleTrees.add(x to y)
                } else {
                    continue
                }
                previous = row[x]
            }
        }

        for (y in forest.indices) {
            var previous = '0' - 1
            for (x in forest.indices) {
                if (forest[x][y] > previous) {
                    visibleTrees.add(y to x)
                } else {
                    continue
                }
                previous = forest[x][y]
            }
            previous = '0' - 1
            for (x in forest.lastIndex downTo 0) {
                if (forest[x][y] > previous) {
                    visibleTrees.add(y to x)
                } else {
                    continue
                }
                previous = forest[x][y]
            }
        }
        return visibleTrees.size
    }

    fun scenicScore(grid: List<List<Char>>, row: Int, col: Int): Int {
        var score = mutableListOf(0, 0, 0, 0)
        val tree = grid[col][row]

        for (x in col - 1 downTo 0) {
            // up
            score[2]++
            if (tree <= grid[x][row]) break
        }
        for (y in row - 1 downTo 0) {
            // left
            score[1]++
            if (tree <= grid[col][y]) break
        }
        for (y in row + 1 until grid[row].size) {
            // right
            score[0]++
            if (tree <= grid[col][y]) break
        }
        for (x in col + 1 until grid.size) {
            // down
            score[3]++
            if (tree <= grid[x][row]) break
        }
        return score.reduce { acc, t -> acc * t }
    }

    fun part2(input: List<String>): Int {
        val forest = input.map { it.toList() }
        var maxScore = 0
        for (x in forest.indices) {
            for (y in forest[x].indices) {
                maxScore = maxOf(maxScore, scenicScore(forest, x, y))
            }
        }
        return maxScore
    }

    val input = readInput("Day08")
    println(part1(input))
    println(part2(input))
}
