fun main() {

    fun getCrates(input: List<String>) : MutableMap<String, ArrayDeque<String>> {
        val regCrate = Regex("[A-Z]")
        return input.takeWhile { regCrate.containsMatchIn(it) }.map { it.chunked(4) }
            .fold(mutableMapOf()) { acc, t ->
                t.forEachIndexed { index, c ->
                    val j = (index + 1).toString()
                    if (c.any { it.isLetter() }) {
                        if (j !in acc.keys) acc[j] = ArrayDeque()
                        acc[j]?.addFirst(c)
                    }
                }
                acc
            }
    }

    fun part1(input: List<String>): String {
        val regMove = Regex("[0-9]+")
        val crates = getCrates(input)

        input.takeLastWhile { it.startsWith("move") }
            .map { l -> regMove.findAll(l).toList().map { it.value } }
            .forEach {
                val (nrToMove, from, to) = it
                val count = nrToMove.toInt()
                crates[to]!!.addAll(crates[from]!!.takeLast(count).reversed())
                repeat(count) { crates[from]!!.removeLast() }
            }
        return crates.toSortedMap().map { it.value.last() }.joinToString().filter { it.isLetter() }
    }


    fun part2(input: List<String>): String {
        val regMove = Regex("[0-9]+")
        val crates = getCrates(input)

        input.takeLastWhile { it.startsWith("move") }
            .map { l -> regMove.findAll(l).toList().map { it.value } }
            .forEach {
                val (nrToMove, from, to) = it
                val count = nrToMove.toInt()
                crates[to]!!.addAll(crates[from]!!.takeLast(count))
                repeat(count) { crates[from]!!.removeLast() }
            }
        return crates.toSortedMap().map { it.value.last() }.joinToString().filter { it.isLetter() }
    }

    val input = readInput("Day05")
    println(part1(input))
    println(part2(input))
}
