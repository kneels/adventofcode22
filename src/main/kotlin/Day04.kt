fun main() {
    fun part1(input: List<String>) = input.map { p -> p.split(',')
            .map { x -> x.split('-').let { (it.first().toInt()..it.last().toInt()).toSet() } } }
            .count { it.first().containsAll(it.last()) || it.last().containsAll(it.first()) }

    fun part2(input: List<String>) = input.map { p -> p.split(',')
        .map { x -> x.split('-').let { (it.first().toInt()..it.last().toInt()).toSet() } } }
        .count { it.first().intersect(it.last()).isNotEmpty() }

    val input = readInput("Day04")
    println(part1(input))
    println(part2(input))
}
