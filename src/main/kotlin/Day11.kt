data class Monkey(
    val items: ArrayDeque<Long>,
    val operation: (Long) -> Long,
    val test: (Long) -> Int,
    var inspections: Long = 0,
)

fun getMonkeys(input: List<String>): List<Monkey> {
    return input.chunked(7).map { m ->
        val startingItems = m[1].substring(18).split(',').map { it.trim().toLong() }
        val operands = m[2].substring(23).split(' ')
        val operation = when (operands[0]) {
            "*" -> { x: Long -> x * (operands[1].toLongOrNull() ?: x) }
            "+" -> { x: Long -> x + (operands[1].toLongOrNull() ?: x) }
            else -> { x: Long -> x }
        }
        val divBy = m[3].substring(21).toLong()
        val ifTrue = m[4].substring(29).toInt()
        val ifFalse = m[5].substring(30).toInt()
        val test = { x: Long -> if (x % divBy == 0L) ifTrue else ifFalse }

        Monkey(ArrayDeque(startingItems), operation, test)
    }
}

fun main() {
    fun part1(monkeys: List<Monkey>): Long {
        repeat(20) {
            for (monke in monkeys) {
                monke.inspections += monke.items.size
                while (monke.items.isNotEmpty()) {
                    val item = monke.items.removeFirst()
                    val newItem = monke.operation(item) / 3
                    val recipient = monke.test(newItem)
                    monkeys[recipient].items.addLast(newItem)
                }
            }
        }
        return monkeys.map { it.inspections }.sorted().takeLast(2).reduce(Long::times)
    }

//    fun part2(monkeys: List<Monkey>): Int {
//
//        return 0
//    }

    println(part1(getMonkeys(readInput("Day11"))))
//    println(part2(getMonkeys(readInput("Day11"))))
}
