fun main() {
    // A / X = ROCK (1 points)
    // B / Y = PAPER (2 points)
    // C / Z = SCISSOR (3 points)
    // The score for a single round is the score for the shape you selected
    // (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round
    // (0 if you lost, 3 if the round was a draw, and 6 if you won).
    val scoreP1 = mapOf(
        "A X" to 4, // DRAW
        "A Y" to 8, // WIN
        "A Z" to 3, // LOSE
        "B X" to 1, // LOSE
        "B Y" to 5, // DRAW
        "B Z" to 9, // WIN
        "C X" to 7, // WIN
        "C Y" to 2, // LOSE
        "C Z" to 6, // DRAW
    )

    // X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win
    val scoreP2 = mapOf(
        "A X" to 3, // NEED TO LOSE
        "A Y" to 4, // NEED TO DRAW
        "A Z" to 8, // NEED TO WIN
        "B X" to 1, // NEED TO LOSE
        "B Y" to 5, // NEED TO DRAW
        "B Z" to 9, // NEED TO WIN
        "C X" to 2, // NEED TO LOSE
        "C Y" to 6, // NEED TO DRAW
        "C Z" to 7, // NEED TO WIN
    )

    fun part1(input: List<String>): Int {
        return input.sumOf { scoreP1[it]!! }
    }

    fun part2(input: List<String>): Int {
        return input.sumOf { scoreP2[it]!! }
    }

    val input = readInput("Day02")
    println(part1(input))
    println(part2(input))
}
