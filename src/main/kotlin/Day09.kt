import java.awt.Point

operator fun Point.minus(other: Point) = Point(x - other.x, y - other.y)
operator fun Point.plus(other: Point) = Point(x + other.x, y + other.y)

fun printGrid(head: Point, tail : Point) {
    for (y in 4 downTo 0) {
        for (x in 0 until 6) {
            if (x == head.x && y == head.y) {
                print("H")
            } else if (x == tail.x && y == tail.y) {
                print("T")
            } else {
                print(".")
            }
        }
        println("")
    }
    println("")
}

val lookup = mapOf(
    Point(-2, 1) to Point(-1, 0),
    Point(-2, 0) to Point(-1, 0),
    Point(-2, -1) to Point(-1, 0),

    Point(-1, -2) to Point(0, -1),
    Point(0, -2) to Point(0, -1),
    Point(1, -2) to Point(0, -1),

    Point(2, 1) to Point(1, 0),
    Point(2, 0) to Point(1, 0),
    Point(2, -1) to Point(1, 0),

    Point(-1, 2) to Point(0, 1),
    Point(0, 2) to Point(0, 1),
    Point(1, 2) to Point(0, 1),
)

fun step(head: Point, tail: Point): Point {
    val delta = (head - tail)
    return lookup[delta]?.let { head - it } ?: tail
}

fun main() {

    fun part1(input: List<String>): Int {
        val head = Point()
        var tail = Point()
        val tailPositions = mutableSetOf(tail)
        for (line in input) {
            val amount = line.substring(2).toInt()
            for (i in 0 until amount) {
                when (line[0]) {
                    'U' -> head.y++
                    'D' -> head.y--
                    'L' -> head.x--
                    'R' -> head.x++
                }
                tail = step(head, tail)
                tailPositions.add(tail)
            }
        }
        return tailPositions.size
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    val input = readInput("Day09")
    println(part1(input))
//    println(part2(input))
}
