import kotlin.math.abs

fun main() {
    data class Cube(val x: Int, val y: Int, val z: Int) {

        private fun manhattanDistance(aCube: Cube, otherCube: Cube): Int {
            return abs(aCube.x - otherCube.x) + abs(aCube.y - otherCube.y) + abs(aCube.z - otherCube.z)
        }

        fun surfacesExposed(otherCubes: List<Cube>): Int {
            var count = 6
            for (cube in otherCubes) {
                if (cube == this) continue

                if (manhattanDistance(this, cube) == 1) count--
            }
            return count
        }
    }

    fun part1(input: List<String>): Int {
        val cubes = input.map {
            val (x, y, z) = it.split(',')
            Cube(x.toInt(), y.toInt(), z.toInt())
        }

        return cubes.sumOf { it.surfacesExposed(cubes) }
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    val input = readInput("Day18")
    println(part1(input))
//    println(part2(input))
}
