import kotlin.math.abs

typealias GridPosition = Pair<Int, Int>

fun main() {
    val MAX_SCORE = 99999999

    data class Node(val x: Int, val y: Int, var f: Int = 0)

    class Grid(private val costMap: List<List<Char>>) {
        private val heightRange: IntRange = costMap.indices
        private val widthRange: IntRange = costMap.first().indices
        private val validMoves = listOf(
            Pair(1, 0),
            Pair(-1, 0),
            Pair(0, 1),
            Pair(0, -1),
        )

        fun heuristicDistance(start: GridPosition, finish: GridPosition): Int {
            val dx = abs(start.first - finish.first)
            val dy = abs(start.second - finish.second)
            return (dx + dy) + (-2) * minOf(dx, dy)
        }

        fun getNeighbours(position: GridPosition): List<GridPosition> = validMoves
            .map { GridPosition(position.first + it.first, position.second + it.second) }
            .filter { inGrid(it) }

        fun moveCost(fromPos: GridPosition, toPos: GridPosition): Int {
            val from = costMap[fromPos.second][fromPos.first]
            val to = costMap[toPos.second][toPos.first]
            return if (to - from > 1) MAX_SCORE else 1
        }

        private fun inGrid(it: GridPosition) = (it.first in widthRange) && (it.second in heightRange)

    }

    /**
     * Implementation of the A* Search Algorithm to find the optimum path between 2 points on a grid.
     *
     * The Grid contains the details of the barriers and methods which supply the neighboring vertices and the
     * cost of movement between 2 cells.  Examples use a standard Grid which allows movement in 8 directions
     * (i.e. includes diagonals) but alternative implementation of Grid can be supplied.
     *
     */
    fun aStarSearch(start: GridPosition, finish: GridPosition, grid: Grid): Pair<List<GridPosition>, Int> {

        /**
         * Use the cameFrom values to Backtrack to the start position to generate the path
         */
        fun generatePath(currentPos: GridPosition, cameFrom: Map<GridPosition, GridPosition>): List<GridPosition> {
            val path = mutableListOf(currentPos)
            var current = currentPos
            while (cameFrom.containsKey(current)) {
                current = cameFrom.getValue(current)
                path.add(0, current)
            }
            return path.toList()
        }

        val openVertices = mutableSetOf(start)
        val closedVertices = mutableSetOf<GridPosition>()
        val costFromStart = mutableMapOf(start to 0)
        val estimatedTotalCost = mutableMapOf(start to grid.heuristicDistance(start, finish))

        val cameFrom = mutableMapOf<GridPosition, GridPosition>()  // Used to generate path by back tracking

        while (openVertices.size > 0) {

            val currentPos = openVertices.minBy { estimatedTotalCost.getValue(it) }

            // Check if we have reached the finish
            if (currentPos == finish) {
                // Backtrack to generate the most efficient path
                val path = generatePath(currentPos, cameFrom)
                return Pair(path, estimatedTotalCost.getValue(finish)) // First Route to finish will be optimum route
            }

            // Mark the current vertex as closed
            openVertices.remove(currentPos)
            closedVertices.add(currentPos)

            grid.getNeighbours(currentPos)
                .filterNot { closedVertices.contains(it) }  // Exclude previous visited vertices
                .forEach { neighbour ->
                    val score = costFromStart.getValue(currentPos) + grid.moveCost(currentPos, neighbour)
                    if (score < costFromStart.getOrDefault(neighbour, MAX_SCORE)) {
                        if (!openVertices.contains(neighbour)) {
                            openVertices.add(neighbour)
                        }
                        cameFrom[neighbour] = currentPos
                        costFromStart[neighbour] = score
                        estimatedTotalCost[neighbour] = score + grid.heuristicDistance(neighbour, finish)
                    }
                }
        }

        return emptyList<GridPosition>() to MAX_SCORE
    }

    fun getCostMap(input: List<String>): Triple<List<List<Char>>, Node, Node> {
        val grid = mutableListOf<MutableList<Char>>()
        var startPos = Node(0, 0)
        var endPos = Node(0, 0)
        input.forEachIndexed { y, sx ->
            grid.add(mutableListOf())
            sx.forEachIndexed { x, c ->
                when (c) {
                    'S' -> { startPos = Node(x, y); grid[y].add('a') }
                    'E' -> { endPos = Node(x, y); grid[y].add('z') }
                    else -> grid[y].add(c)
                }
            }
        }
        return Triple(grid, startPos, endPos)
    }

    fun part1(input: List<String>): Int {
        val (costMap, startPos, endPos) = getCostMap(input)

        val (_, cost) = aStarSearch(
            GridPosition(startPos.x, startPos.y),
            GridPosition(endPos.x, endPos.y),
            Grid(costMap)
        )
        return cost
    }

    fun part2(input: List<String>): Int {
        val startingPositions = mutableSetOf<GridPosition>()
        val costMap = mutableListOf<MutableList<Char>>()
        var endPos = Node(0, 0)
        input.forEachIndexed { y, sx ->
            costMap.add(mutableListOf())
            sx.forEachIndexed { x, c ->
                when (c) {
                    'S' -> { startingPositions.add(GridPosition(x, y)); costMap[y].add('a') }
                    'E' -> { endPos = Node(x, y); costMap[y].add('z') }
                    'a' -> { startingPositions.add(GridPosition(x, y)); costMap[y].add(c) }
                    else -> costMap[y].add(c)
                }
            }
        }

        return startingPositions.minOf {
            val (_, cost) = aStarSearch(
                it,
                GridPosition(endPos.x, endPos.y),
                Grid(costMap)
            )
            cost
        }
    }

    val input = readInput("Day12")
    println(part1(input))
    println(part2(input))
}


