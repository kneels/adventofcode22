fun main() {
    val prio = ('a'..'z').plus(('A'..'Z')).zip((1..52)).toMap()

    fun part1(input: List<String>) =
        input.flatMap { l -> l.chunked(l.length / 2) { it.toSet() }.reduce { acc, s -> acc.intersect(s) } }
            .sumOf { prio[it] ?: 0 }

    fun part2(input: List<String>) =
        input.chunked(3) { c -> c.map { it.toSet() }.reduce { acc, s -> acc.intersect(s) } }
            .sumOf { prio[it.first()] ?: 0 }

    val input = readInput("Day03")
    println(part1(input))
    println(part2(input))
}
