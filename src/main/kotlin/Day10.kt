fun sample(x: Int, cycle: Int) : Int {
    if (cycle in listOf(20, 60, 100, 140, 180, 220)) {
        return cycle * x
    }
    return 0
}

fun main() {
    fun part1(input: List<String>): Int {
        var x = 1
        var cycle = 0
        var sum = 0
        for (line in input) {
            if (line == "noop") {
                cycle++
                sum += sample(x, cycle)
            } else {
                val amount = line.substring(5).toInt()
                cycle++
                sum += sample(x, cycle)
                cycle++
                sum += sample(x, cycle)
                x += amount
            }
        }
        return sum
    }

    fun part2(input: List<String>): Int {
        return input.size
    }

    val input = readInput("Day10")
    println(part1(input))
//    println(part2(input))
}
