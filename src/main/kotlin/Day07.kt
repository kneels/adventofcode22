fun main() {

    data class Node(val parent: Node?, val name: String, val isFile: Boolean, val size: Long = 0L) {
        val children = mutableSetOf<Node>()

        fun addFile(fileName: String, size: Long): Node {
            val node = Node(this, fileName, true, size = size)
            children.add(node)
            return node
        }

        fun addDir(dirName: String): Node {
            val node = Node(this, dirName, false)
            children.add(node)
            return node
        }

        fun goDownTo(dirName: String): Node? {
            return children.firstOrNull { it.name == dirName }
        }

        fun getTotalSize(): Long {
            var size = size
            children.forEach {
                if (it.isFile) {
                    size += it.size
                } else {
                    size += it.getTotalSize()
                }
            }
            return size
        }

        fun getAllSubDirectories(): List<Node> {
            val dirs = mutableListOf<Node>()
            children.forEach {
                if (!it.isFile) {
                    dirs.add(it)
                    dirs.addAll(it.getAllSubDirectories())
                }
            }
            return dirs
        }
    }

    fun getRoot(input: List<String>): Node {
        val root = Node(null, "ROOT", false)
        var cursor = root

        input.forEach {
            if (it == "\$ cd ..") {
                cursor = cursor.parent!!
            } else if (it.startsWith("\$ cd ")) {
                val to = it.substring(5)
                cursor = cursor.goDownTo(to) ?: cursor.addDir(to)
            } else if (it.startsWith("dir")) {
                // content dir
                cursor.addDir(it.substring(4))
            } else if (it.first().isDigit()) {
                // content file
                val (size, name) = it.split(' ')
                cursor.addFile(name, size.toLong())
            }
        }
        return root
    }

    fun part1(root: Node): Long {
        return root.getAllSubDirectories().map { it.getTotalSize() }.filter { it <= 100000 }.sumOf { it }
    }

    fun part2(root: Node): Long {
        val spaceNeeded = 30000000 - (70000000L - root.getTotalSize())
        return root.getAllSubDirectories().map { it.getTotalSize() }.filter { it >= spaceNeeded }.minByOrNull { it }!!
    }

    val input = readInput("Day07")
    println(part1(getRoot(input)))
    println(part2(getRoot(input)))
}
