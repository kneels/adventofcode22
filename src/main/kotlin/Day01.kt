import java.io.File
import java.lang.Integer.max

fun main() {
    fun part1(input: List<String>): Int {
        val iter = input.iterator()
        var max = 0
        while (iter.hasNext()) {
            val total = iter.asSequence().takeWhile { it.isNotEmpty() }.map { it.toInt() }.sum()
            max = max(total, max)
        }

        return max
    }

    fun part1Golf(input: List<String>): Int {
        return input.fold(mutableListOf(mutableListOf<Int>())) { acc, t ->
            if (t.isBlank()) acc.add(mutableListOf())
            else acc.last().add(t.toInt())
            acc
        }.maxOf { it.sum() }
    }

    fun part1Golf2() = File("src/inputs/Day01.txt").readText().trim().split("\r\n\r\n")
        .maxOf { l -> l.split("\r\n").sumOf { it.toInt() } }

    fun part2(input: List<String>): Int {
        val iter = input.iterator()
        val sums = mutableListOf<Int>()
        while (iter.hasNext()) {
            val total = iter.asSequence().takeWhile { it.isNotEmpty() }.map { it.toInt() }.sum()
            sums.add(total)
        }

        return sums.sortedDescending().take(3).sum()
    }

    val input = readInput("Day01")
    println(part1(input))
    println(part1Golf(input))
    println(part1Golf2())
    println(part2(input))
}
